function buscarBebida() {
    const nombreBebida = document.getElementById('txtNombre').value;

    if (nombreBebida.trim() === '') {
        alert('Por favor, ingrese el nombre de la bebida.');
        return;
    }

    axios.get('https://www.thecocktaildb.com/api/json/v1/1/search.php?s=' + nombreBebida)
        .then(function (response) {
            if (response.data.drinks && response.data.drinks.length > 0) {
                mostrarDetalles(response.data.drinks[0]);
            } else {
                alert('Bebida no encontrada.');
            }
        })
        .catch(function (error) {
            console.error('Surgió un error:', error);
        });
}

function mostrarDetalles(drink) {
    document.getElementById('txtCategoria').value = drink.strCategory;
    document.getElementById('txtInstrucciones').value = drink.strInstructions;

    const imagenBebida = document.getElementById('imagenBebida');
    
    imagenBebida.innerHTML = '<img style="width: 400px; height: 400px;" src="' + drink.strDrinkThumb + '" alt="Drink Image">';
}

function limpiarCampos() {
    document.getElementById('txtNombre').value = '';
    document.getElementById('txtCategoria').value = '';
    document.getElementById('txtInstrucciones').value = '';
    document.getElementById('imagenBebida').innerHTML = '';
}

document.getElementById('btnBuscar').addEventListener('click', buscarBebida);
document.getElementById('btnLimpiar').addEventListener('click', limpiarCampos);